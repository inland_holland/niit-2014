# include <string.h>                        
# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# define N 256


int getWords (char**str,char *string_words,int len)
{
  int k=0,i=0;
  while(i < len)
  {  
    if(string_words[i]!=' ')
    {
      str[k]=&string_words[i];
      k++;
      while(i < len && string_words[i]!=' ')i++;
    }
    i++;                                                          
  }
  return k; //количество слов
}
 
void printWord (char **str,int k) //вывод слов в случайном порядке
{  
  int k1,k2,temp,i,num[N];
  for (i = 0; i < k; i++)
     num[i]=i;
  for ( i = 0; i < k; i++)
  {
    k1= rand()%k; // случайно выбираем, какие элементы
    k2=rand()%k; //поменять местами
    if (k1!=k2)// чтоб не менять элемент с самим собой
    {
      temp = num[k1];
      num[k1]= num[k2]; // меняем
      num[k2]=temp;
    }
  } 
  for(i=0;i < k;i++)
  {
    while(*str [num[i]]!=0 && *str[num[i]]!=' ')
    printf("%c",*str[num[i]]++);
    printf(" ");  
  }
  printf("\n");              
}

int main(int argc, char *argv[])
{                       
  char string_words[N];
  char *str[N];
  int k;
  int len;
  srand(time(NULL));
  printf("enter string:\n");
  fgets(string_words,N,stdin);
  len=strlen(string_words);
  string_words[len-1]=' ';    
  k=getWords(str,string_words,len);
  printWord(str,k);
  return 0;
}


