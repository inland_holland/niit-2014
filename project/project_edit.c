#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct SYM
  {
    unsigned char ch;
    float freq;
    char code [256];
    struct SYM *left;
    struct SYM *right;
  };

struct SYM* buildTree(SYM *psym[], int N)
  {
    struct SYM *temp=(struct SYM*)malloc(sizeof(struct SYM));
    temp->freq=psym[N-1]->freq+psym[N-2]->freq;
    temp->left=psym[N-1];
    temp->right=psym[N-2];
    temp->code[0]=0;
    int i,j;
    if (N==2)
      return temp;
    else
      {
        for(i=0;i<N;i++)
           if (temp->freq>psym[i]->freq)
             {    
               for(j=N-1;j>i;j--)
                 psym[j]=psym[j-1];                                      
                 psym[i]=temp;
                 break;
             }        
      }
    return buildTree(psym,N-1);
   }

void makeCodes(SYM *root)
  {
    if(root->left)
    {
      strcpy(root->left->code,root->code);
      strcat(root->left->code,"0");
      makeCodes(root->left);
    }
    if(root->right)
    {
      strcpy(root->right->code,root->code);
      strcat(root->right->code,"1");
      makeCodes(root->right);
    }
  }

union code
  {
    unsigned char ch;
    struct byte
     {
       unsigned short b1:1;
       unsigned short b2:1;
       unsigned short b3:1;
       unsigned short b4:1;
       unsigned short b5:1;
       unsigned short b6:1;
       unsigned short b7:1;
       unsigned short b8:1;
     } byte;
  };

int main(int argc,char**argv)
  {
    int ch,i,j,c_tt,a;
    int k=0; // счетчик частоты встречаемости
    int kk=0; // счётчтк количества всех знаков в файле
    int c_temp=0;//счётчик количества символов из 0 и 1 в промежуточном файле temp
    int t_size; //размер хвоста файла
    int amount[256]={0};//инициализируем массив количества уникальных символов
    SYM syms[256]={0}; //инициализируем массив записей типа sym
    SYM *psym[256]; //инициализируем массив указателей на записи
    float sum_freq=0;//сумма частот встречаемости
    int mes[8];//массив 0 и 1
    ///char j=0;//вспомогательная переменная

    FILE *fp_in;
    FILE *fp_101;
    FILE *fp_res;
    if(argc<2)
      return 1;

    fp_in=fopen(argv[1],"rb");
    fp_101=fopen("/home/user/binary","wb");
    fp_res=fopen("/home/user/result","wb");
    
    if(fp_in==NULL)
      {
        puts("FILE NOT OPEN!!!!!!!");
        return 0;
      }

    while((ch=fgetc(fp_in))!=EOF)
      {
        for(j=0;j<256;j++)
           {
             if(ch==syms[j].ch)
             {
                 amount[j]++;
                 kk++;                 
                 break;
             }
             if (syms[j].ch==0)
             {
                 syms[j].ch=(unsigned char)ch;
                 amount[j]=1;
                 k++; kk++;
                 break;
             }             
           }        
      }

    //SYM *syms=(SYM*)malloc(N*sizeof(SYM));
    //SYM **psym=(SYM**)malloc(N*sizeof(SYM*));
    
    for(i=0;i<k;i++)
      syms[i].freq=(float)amount[i]/kk;

        for(i=0;i<k;i++)
           psym[i]=&syms[i];
    SYM tempp;
    for(i=1;i<k;i++)
        for(j=0;j<k-1;j++)
           if(syms[j].freq<syms[j+1].freq)
             {
                 tempp=syms[j];
                 syms[j]=syms[j+1];
                 syms[j+1]=tempp;
             }
        for(i=0;i<k;i++)
           {
              sum_freq+=syms[i].freq;     
              printf("%f %c\n",syms[i].freq,psym[i]->ch,i);
           };
    printf("%d %f\n",kk,sum_freq);
    
    SYM* root=buildTree(psym,k);
    makeCodes(root);
    rewind(fp_in);

    while((ch=fgetc(fp_in))!=EOF)
      {
        for(i=0;i<k;i++)
           if(ch==syms[i].ch)
             fputs(syms[i].code,fp_101);
      }
    fclose(fp_101);

    //int i=0;
    fp_101=fopen("/home/user/binary","rb");
    while((ch=fgetc(fp_101))!=EOF)
        c_temp++;
     
    c_tt=c_temp/8;
    t_size=c_temp%8;
    fwrite("compressing",sizeof(char),4,fp_res);
    fwrite(&k,sizeof(int),1,fp_res);
    fwrite(&t_size,sizeof(int),1,fp_res);
    for(i=0;i<k;i++)
      {
        fwrite(&syms[i].ch,sizeof(char),1,fp_res);
        fwrite(&syms[i].freq,sizeof(float),1,fp_res);
      };
    
    rewind(fp_101);
        
    union code code1;
    j=0;
    printf("%d\n",c_tt);
    for(i=0;i<c_tt;i++)
      {
        mes[j]=fgetc(fp_101);
        if(mes[j]==EOF)
          putchar('*');
        if(j==7)
        {        
           code1.byte.b1=mes[0]-'0';
           code1.byte.b2=mes[1]-'0';
           code1.byte.b3=mes[2]-'0';
           code1.byte.b4=mes[3]-'0';
           code1.byte.b5=mes[4]-'0';
           code1.byte.b6=mes[5]-'0';
           code1.byte.b7=mes[6]-'0';
           code1.byte.b8=mes[7]-'0';
           fputc(code1.ch,fp_res);
           j=-1;
        }
        j++;     
      }; 
    //j=0;
    for(i=0;i<=t_size;i++)
      {
        mes[j]=fgetc(fp_101);
        if(j==t_size)
           {        
             code1.byte.b1=mes[0]-'0';
             code1.byte.b2=mes[1]-'0';
             code1.byte.b3=mes[2]-'0';
             code1.byte.b4=mes[3]-'0';
             code1.byte.b5=mes[4]-'0';
             code1.byte.b6=mes[5]-'0';
             code1.byte.b7=mes[6]-'0';
             code1.byte.b8=mes[7]-'0';
             fputc(code1.ch,fp_res);       
           }
        j++;     
      }     
        
           fcloseall();
  
  return 0; 
    
}
