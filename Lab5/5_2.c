#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define N 30
#define N2 15

void del (char kld[N][N])
{
  int i,j;
  for(i=0;i<N;i++)
  {
    for(j=0;j<N-1;j++)
    {
      kld[i][j]=' ';
    }
  }
}

void fill (char kld[N][N])
{
  int ind,i,j;
  srand(time(0));
  for(i=0;i<N2;i++)
    for(j=i;j<N2;j++)
    {  
      ind=rand()%2;
      if(ind!=0)
        kld[i][j]='*';
    }
  for(j=0;j<N2;j++)
    for(i=j;i<N2;i++)
       kld[i][j]=kld[j][i];
}

void copy (char kld[N][N])
{
  int i,j;
  for(i=0;i<N2;i++)
    for(j=0;j<N2;j++)
      kld[N-1-i][N2-1-j]=kld[N-1-i][N2+j]=kld[i][N2+j]=kld[i][N2-1-j];
}

void print (char kld[N][N])
{
  int i,j;
  for(i=0;i<N;i++)
  {  
    printf("\n");
    for(j=0;j<N;j++)
      printf("%c",kld[i][j]);
  }
}

int main()
{ 
  clock_t begin;
  char kld[N][N]={0};
  while(1)
  {   
    del(kld);
    fill(kld);
    copy(kld);
    print(kld);
    begin=clock();
    while(clock()<begin+CLOCKS_PER_SEC);
  }
return 0;
}
