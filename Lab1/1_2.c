#include <stdio.h>
int main ()
{
 int hours, minutes, seconds;
 char separator;

 printf ("enter the time in HH:MM:SS. \n");
 scanf ("%d%c%d%c%d", &hours, &separator, &minutes, &separator, &seconds);
 if (hours>=24 || hours<0 || minutes>=60 || minutes<0 || seconds>=60 || seconds<0)
   {
    printf ("it's only 24 hours in a day, 60 minutes in an hour and 60 seconds in a minute on the Earth. \n");
    return 0;
   }
 if (hours>=6 && hours<12)
   printf ("good morning! \n");
  else if (hours>=12 && hours<18)
       printf ("good afternoon! \n");
     else if (hours>=18 && hours<24)
            printf ("good evening! \n");
         else
             printf ("goodnight! \n");
return 0;
}
