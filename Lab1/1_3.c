#include <stdio.h>
int main ()
{
 float angle, convangle, pi=3.1415926;
 char unit, convunit;
 printf ("enter an angle. if you mean radians - enter R after value, if you mean degrees - enter G after value. \n");
 scanf ("%f%c", &angle, &unit);
 
 switch (unit)
 {
  case 'R':
      convangle=(angle*180)/pi;
      convunit='G';
  break;
  case 'G':
      convangle=(angle*pi)/180;
      convunit='R';
  }
 printf ("you entered %.2f %c, it is %.2f %c. \n", angle, unit, convangle, convunit);
return 0;
}
